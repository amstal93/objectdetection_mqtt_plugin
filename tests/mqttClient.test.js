'use strict'

const mockClient = {
  end: jest.fn(),
  publish: jest.fn(async (topic, message) => {
    return undefined
  })
}

const mockConnect = {
  connectAsync: jest.fn(async (url, options, retry) => {
    return mockClient
  })
}

const fs = require('fs')
const mqttClient = require('../src/libs/utils/src/lib/mqttClient')

jest.mock('async-mqtt', () => mockConnect)

describe('mqtt constructor tests', () => {
  test('MQTT client constructor', () => {
    const caFilePath = `${__dirname}/config/ca_certificates/mosquitto.org.crt`

    const expectedOptions = {
      ca: fs.readFileSync(caFilePath),
      rejectUnauthorized: true,
      connectTimeout: 30000
    }
    const expectedUrl = 'mqtts://test.mosquitto.org:8883'

    const client = new mqttClient._testable.MQTTClient(expectedUrl, caFilePath)

    expect(client.url).toEqual(expectedUrl)
    expect(client.options).toEqual(expectedOptions)
  })

  test('MQTT client constructor throws error when ca file path is invalid', () => {
    const caFile = undefined

    let client
    let error = false
    try {
      client = new mqttClient._testable.MQTTClient('mqtts://test.mosquitto.org:8883', caFile)
    } catch (err) {
      error = true
    }

    expect(error).toBeTruthy()
    expect(client).toBeUndefined()
  })

  test('MQTT client constructor overrides default rejectUnauthorised parameter when supplied', () => {
    const caFilePath = `${__dirname}/config/ca_certificates/mosquitto.org.crt`
    const expectedUnauthorised = false
    const expectedUrl = 'mqtts://test.mosquitto.org:8883'

    const expectedOptions = {
      ca: fs.readFileSync(caFilePath),
      rejectUnauthorized: expectedUnauthorised,
      connectTimeout: 30000
    }

    const client = new mqttClient._testable.MQTTClient(expectedUrl, caFilePath, expectedUnauthorised)

    expect(client.url).toEqual(expectedUrl)
    expect(client.options).toEqual(expectedOptions)
  })

  test('MQTT client constructor overrides default timeout parameter when supplied', () => {
    const caFilePath = `${__dirname}/config/ca_certificates/mosquitto.org.crt`
    const expectedTimeout = 20000
    const expectedUnauthorised = false
    const expectedUrl = 'mqtts://test.mosquitto.org:8883'

    const expectedOptions = {
      ca: fs.readFileSync(caFilePath),
      rejectUnauthorized: expectedUnauthorised,
      connectTimeout: expectedTimeout
    }

    const client = new mqttClient._testable.MQTTClient(expectedUrl, caFilePath, expectedUnauthorised, expectedTimeout)

    expect(client.url).toEqual(expectedUrl)
    expect(client.options).toEqual(expectedOptions)
  })
})

describe('mqtt tests', () => {
  afterAll(() => {
    jest.resetModules()
  })

  beforeEach(() => {
    jest.clearAllMocks()
  })

  test('MQTT client created with valid url and ca certificate', () => {
    const caFile = `${__dirname}/config/ca_certificates/mosquitto.org.crt`
    const client = mqttClient('mqtts://test.mosquitto.org:8883', caFile)

    expect(client).toBeDefined()
    expect(client.constructor.name).toEqual('MQTTClient')
  })

  test('MQTT client publish message', async () => {
    expect.assertions(1)

    const caFile = `${__dirname}/config/ca_certificates/mosquitto.org.crt`
    const client = mqttClient('mqtts://test.mosquitto.org:8883', caFile, false)

    await expect(client.publish('test/topic', 'a short test')).resolves.toBeUndefined()
  })

  test('MQTT Client throws error when no ca certificate specified', () => {
    const caFile = undefined
    expect(() => { mqttClient('mqtts://test.mosquitto.org:8883', caFile) }).toThrowError('CA certificate must be specified')
  })

  test('MQTT Client publish throws error when fails to publish due to invalid connection', async () => {
    const caFile = `${__dirname}/config/ca_certificates/mosquitto.org.crt`
    mockConnect.connectAsync.mockImplementationOnce(async (topic, message) => {
      throw new Error('CONNECTION ERROR')
    })

    const client = mqttClient('mqtts://localhost:9999', caFile)

    await expect(client.publish('test/topic', 'a short test'))
      .rejects
      .toHaveProperty(
        'message',
        'Error: CONNECTION ERROR'
      )
  })

  test('MQTT Client publish throws error when async-mqtt fails to publish', async () => {
    const caFile = `${__dirname}/config/ca_certificates/mosquitto.org.crt`
    mockClient.publish.mockImplementationOnce(async (topic, message) => {
      throw new Error('PUBLISH ERROR')
    })

    const client = mqttClient('mqtts://test.mosquitto.org:8883', caFile, false)

    await expect(client.publish('test/topic', 'a short test'))
      .rejects
      .toHaveProperty(
        'message',
        'PUBLISH ERROR'
      )
  })
})

describe('mqtt module tests', () => {
  beforeEach(() => {
    jest.resetModules()
  })
  test('_testable exports defined when NODE_ENV is test', () => {
    const mqttClient = require('../src/libs/utils/src/lib/mqttClient')

    expect(mqttClient._testable).toBeDefined()
  })

  test('_testable is not exported when NODE_ENV is not test', () => {
    const curNodeEnv = process.env.NODE_ENV

    process.env.NODE_ENV = 'dev'
    try {
      const mqttClient = require('../src/libs/utils/src/lib/mqttClient')

      expect(mqttClient._testable).toBeUndefined()
    } finally {
      process.env.NODE_ENV = curNodeEnv
    }
  })
})
