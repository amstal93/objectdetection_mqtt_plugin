'use strict'

const mockChild = {
  on: jest.fn()
}

const mockFork = {
  fork: jest.fn().mockImplementation((scriptPath, args, options) => {
    return mockChild
  })
}

require('child_process')

const { forkChildProcess } = require('../src/libs/utils/src/lib/process')

/**
 * Return true if in a debugging environment
 * @returns True when --inspect-brk or inspect is present in process.execArgv
 */
function isDebugging () {
  let count = 0
  let found = false

  const re = /^--(?:inspect-brk|inspect){1}(?:=\d+)?$/

  while (count < process.execArgv.length && !found) {
    if (re.test(process.execArgv[count])) {
      found = true
    }
    count++
  }

  return found
}

jest.mock('child_process', () => mockFork)

describe('debugging environment tests', () => {
  beforeEach(() => {
    jest.resetModules()
    jest.restoreAllMocks()
  })

  test('--inspect-brk option set in execArgv of forked child when in debugging environment', async () => {
    const existingExecArgs = [...process.execArgv]
    const scriptPath = `${__dirname}/../../../src/notify.js`

    const host = process.env.MQTT_HOST || 'localhost'

    const config = {
      mqtt: {
        cafile: `${__dirname}/../../docker/certs/localCA.crt`,
        host: host,
        password: 'password',
        rejectUnauthorised: 'false',
        user: 'user'
      },
      objectDetection: {
        minConfidence: 0.70,
        objectClasses: 'person',
        timeoutPeriod: 5000
      }
    }

    const expectedArgv = { execArgv: ['--inspect-brk=5555'] }

    const expectedProcessArgs = [
      `mqtts://${config.mqtt.user}:${config.mqtt.password}@${config.mqtt.host}:8883`,
      config.mqtt.cafile,
      config.mqtt.rejectUnauthorised,
      config.objectDetection.timeoutPeriod
    ]

    if (!isDebugging()) {
      process.execArgv.push('--inspect-brk')
    }

    mockFork.fork.mockImplementationOnce((scriptPath, args, options) => {
      throw new Error('mock Error')
    })

    let child
    try {
      child = await forkChildProcess(config, scriptPath)
    } catch (error) {
      expect(mockFork.fork).toBeCalledTimes(1)
      expect(mockFork.fork).toBeCalledWith(scriptPath, expectedProcessArgs, expectedArgv)
      expect(child).toBeUndefined()
    } finally {
      process.execArgv = [...existingExecArgs]
    }
  })
})
