'use strict'

// jest.config.js
module.exports = {
  collectCoverage: true,
  collectCoverageFrom: [
    '<rootDir>/src/**/*.js',
    '!<rootDir>/node_modules/'
  ],
  coverageDirectory: '<rootDir>/tests/coverage/unit',
  coveragePathIgnorePatterns: [ // shinobi-tensorflow plugin code is from Shinobi, Moe Allam
    '<rootDir>/src/ObjectDetectors.js',
    '<rootDir>/src/shinobi-tensorflow.js'
  ],
  coverageThreshold: {
    global: {
      branches: 85,
      functions: 85,
      lines: 85,
      statements: 85
    }
  },
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputDirectory: '<rootDir>/tests/coverage/unit'
      }
    ]
  ],
  globalSetup: '<rootDir>/tests/setup/globalSetup.js',
  modulePathIgnorePatterns: ['<rootDir>/tests/e2e'],
  rootDir: `${__dirname}/..`,
  verbose: true
}
