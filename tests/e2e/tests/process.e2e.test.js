const mqtt = require('async-mqtt')

const { forkChildProcess } = require('../../../src/libs/utils/src')
const { dispose, countMessageReceivedAsync, loadJSON, wait } = require('../../utils')

describe('processFork tests', () => {
  test('notify detection message published on MQTT for no prior detections', async done => {
    const config = {
      mqtt: {
        cafile: `${__dirname}/../../docker/certs/localCA.crt`,
        host: `${global.config.host}`,
        password: `${global.config.password}`,
        rejectUnauthorised: 'false',
        user: `${global.config.user}`
      },
      objectDetection: {
        minConfidence: 0.70,
        objectClasses: 'person',
        timeoutPeriod: 5000
      }
    }
    const expectedMessage = await loadJSON(`${__dirname}/../../data/personDetection.json`)
    const expectedTopic = `shinobi/${expectedMessage.group}/${expectedMessage.monitorId}/trigger`

    let mqttClient
    let child

    try {
      mqttClient = await mqtt.connectAsync(`${global.config.mqttUrl}`)

      child = await forkChildProcess(config, `${__dirname} /../../../src/notify.js`)

      child.send(expectedMessage)

      await mqttClient.subscribe(`${expectedTopic}`)

      await expect(countMessageReceivedAsync(mqttClient, expectedMessage, expectedTopic, 1)).resolves.toBeUndefined()
    } catch (error) {
      console.error(`${error}`)
    } finally {
      if (mqttClient) {
        await dispose(mqttClient, child)
        expect(mqttClient.connected).toBeFalsy()
      }
    }

    done()
  })

  test('notify detection message published on MQTT after timer for cam has expired', async () => {
    const config = {
      mqtt: {
        cafile: `${__dirname}/../../docker/certs/localCA.crt`,
        host: `${global.config.host}`,
        password: `${global.config.password}`,
        rejectUnauthorised: 'false',
        user: `${global.config.user}`
      },
      objectDetection: {
        minConfidence: 0.70,
        objectClasses: 'person',
        timeoutPeriod: 5000
      }
    }

    // load test message and connect to mqtt broker
    const expectedMessage = await loadJSON(`${__dirname}/../../data/personDetection.json`)
    const expectedTopic = `shinobi/${expectedMessage.group}/${expectedMessage.monitorId}/trigger`

    const mqttClient = await mqtt.connectAsync(`${global.config.mqttUrl}`)
    await mqttClient.subscribe(`${expectedTopic}`)

    // get a promise that resolves when 2 messages received
    const promise = countMessageReceivedAsync(mqttClient, expectedMessage, expectedTopic, 2)

    // fork the child waiting until it has been initialised
    const child = await forkChildProcess(config, `${__dirname}/../../../src/notify.js`)

    // send the message twice with an interval of 7 seconds, could have used setInterval here?
    child.send(expectedMessage, (err) => {
      expect(err).toBeNull()
    })

    await wait(7)

    // send the message again
    child.send(expectedMessage, (err) => {
      expect(err).toBeNull()
    })

    await wait(7)

    // dispose the mqtt client and child process
    promise
      .then(async () => { })
      .catch((error) => { console.error(`${error}`) })
      .finally(async () => {
        await dispose(mqttClient, child)
        expect(mqttClient.connected).toBeFalsy()
        expect(child.killed).toBeTruthy()
      })
  }, 16000)

  test('notify detection for camera rejected while timer running', async () => {
    const config = {
      mqtt: {
        cafile: `${__dirname}/../../docker/certs/localCA.crt`,
        host: `${global.config.host}`,
        password: `${global.config.password}`,
        rejectUnauthorised: 'false',
        user: `${global.config.user}`
      },
      objectDetection: {
        minConfidence: 0.70,
        objectClasses: 'person',
        timeoutPeriod: 5000
      }
    }

    // load test message and connect to mqtt broker
    const expectedMessage = await loadJSON(`${__dirname}/../../data/personDetection.json`)
    const expectedTopic = `shinobi/${expectedMessage.group}/${expectedMessage.monitorId}/trigger`

    const mqttClient = await mqtt.connectAsync(`${global.config.mqttUrl}`)
    await mqttClient.subscribe(`${expectedTopic}`)

    // get a promise that resolves when 2 messages received and asserts on expectedMessage and expectTopic
    const promise = countMessageReceivedAsync(mqttClient, expectedMessage, expectedTopic, 2)

    // fork the child waiting until it has been initialised
    const child = await forkChildProcess(config, `${__dirname}/../../../src/notify.js`)

    // Send MSGa | Wait(Timeout) | Send MSGb | Wait(1sec) | Send MSGc | Wait(Timeout)
    child.send(expectedMessage)
    await wait(7)
    child.send(expectedMessage)
    await wait(1)
    child.send(expectedMessage)
    await wait(7)

    // dispose the mqtt client and child process
    promise
      .then(async () => { })
      .catch((error) => { console.error(`${error}`) })
      .finally(async () => {
        await dispose(mqttClient, child)
        expect(mqttClient.connected).toBeFalsy()
        expect(child.killed).toBeTruthy()
      })
  }, 18000)
})
