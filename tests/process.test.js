'use strict'

const { forkChildProcess, Process } = require('../src/libs/utils/src')
const { _testable } = require('../src/libs/utils/src/lib/process')
const { Timer } = require('../src/libs/utils/src/lib/timer')

/**
 * Return true if in a debugging environment
 * @returns True when --inspect-brk or inspect is present in process.execArgv
 */
function isDebugging () {
  let count = 0
  let found = false

  const re = /^--(?:inspect-brk|inspect){1}(?:=\d+)?$/

  while (count < process.execArgv.length && !found) {
    if (re.test(process.execArgv[count])) {
      found = true
    }
    count++
  }

  return found
}

jest.mock('../src/libs/utils/src/lib/mqttClient', () => {
  return jest.fn().mockImplementation((url, caFilePath) => {
    return {
      publish: async (topic, message) => {

      }
    }
  })
})

describe('forkChildProcess tests', () => {
  test('Process is forked with valid configuration, including credentials', async () => {
    const args = {
      mqtt: {
        cafile: `${__dirname}/docker/certs/localCA.crt`,
        host: 'localhost',
        rejectUnauthorised: 'false',
        password: 'password',
        user: 'user',
        port: 8883
      },
      objectDetection: {
        minConfidence: 0.70,
        objectClasses: 'person',
        timeoutPeriod: 5000
      }
    }

    let child
    let errorOccurred = false

    try {
      child = await forkChildProcess(args, './src/notify.js')
    } catch (error) {
      errorOccurred = true
    }

    expect(errorOccurred).toBe(false)
    expect(child).toBeDefined()
    expect(child).toHaveProperty('pid')

    child.kill('SIGTERM')
    expect(child.killed).toBeTruthy()
  })

  test('Process is forked with valid configuration with credentials set from environment variables', async () => {
    const OLD_ENV = process.env

    process.env = { ...OLD_ENV }
    process.env.MQTT_USER = 'user'
    process.env.MQTT_PASSWORD = 'pass'

    const args = {
      mqtt: {
        cafile: `${__dirname}/docker/certs/localCA.crt`,
        host: 'localhost',
        rejectUnauthorised: 'false',
        user: 'anotherUser',
        password: 'anotherPassword'
      },
      objectDetection: {
        minConfidence: 0.70,
        objectClasses: 'person',
        timeoutPeriod: 5000
      }
    }

    let child
    let errorOccurred = false

    try {
      child = await forkChildProcess(args, './src/notify.js')
    } catch (error) {
      errorOccurred = true
    } finally {
      process.env = OLD_ENV
    }

    expect(errorOccurred).toBe(false)
    expect(child).toBeDefined()
    expect(child).toHaveProperty('pid')

    child.kill('SIGTERM')
    expect(child.killed).toBeTruthy()
  })

  test('Error thrown when neither environment variables set or user and password credentials', async () => {
    const OLD_ENV = process.env

    process.env = { ...OLD_ENV }
    delete process.env.MQTT_USER
    delete process.env.MQTT_PASSWORD

    const args = {
      mqtt: {
        cafile: `${__dirname}/docker/certs/localCA.crt`,
        host: 'localhost',
        rejectUnauthorised: 'false',
        port: 8883
      },
      objectDetection: {
        minConfidence: 0.70,
        objectClasses: 'person',
        timeoutPeriod: 5000
      }
    }

    try {
      await expect(forkChildProcess(args, './src/notify.js'))
        .rejects
        .toHaveProperty(
          'message',
          'Failed to construct mqtt connection string: user and password properties not set in mqtt section of config file and MQTT_HOST, MQTT_USER and MQTT_PASSWORD environment variables are not set'
        )
    } finally {
      process.env = OLD_ENV
    }
  })

  test('Error is raised for missing args', async () => {
    await expect(forkChildProcess(undefined, './src/notify.js'))
      .rejects
      .toHaveProperty(
        'message',
        'Arguments are empty'
      )
  })

  test('Error is raised for missing mqtt property', async () => {
    await expect(forkChildProcess({}, './src/notify.js'))
      .rejects
      .toHaveProperty(
        'message',
        'Arguments are missing the mqtt property'
      )
  })

  test('Error is raised for missing mqtt.host property', async () => {
    const args = {
      mqtt: {
        cafile: `${__dirname}/docker/certs/localCA.crt`,
        port: 83333,
        rejectUnauthorised: 'true'
      },
      objectDetection: {

      }
    }
    await expect(forkChildProcess(args, './src/notify.js'))
      .rejects
      .toHaveProperty(
        'message',
        'Missing mqtt.host argument'
      )
  })

  test('Error is raised for missing mqtt.cafile property', async () => {
    const args = {
      mqtt: {
        host: 'locahost',
        port: 83333
      },
      objectDetection: {

      }
    }
    await expect(forkChildProcess(args, './src/notify.js'))
      .rejects
      .toHaveProperty(
        'message',
        'Missing mqtt.cafile argument'
      )
  })

  test('Error is raised for missing mqtt.rejectUnauthorised property', async () => {
    const args = {
      mqtt: {
        cafile: '/path/to/some/ca/file',
        host: 'locahost',
        port: 8333
      },
      objectDetection: {

      }
    }
    await expect(forkChildProcess(args, './src/notify.js'))
      .rejects
      .toHaveProperty(
        'message',
        'Missing mqtt.rejectUnauthorised argument'
      )
  })

  test('Error is raised for missing objectDetection property', async () => {
    const args = {
      mqtt: {
        cafile: '/path/to/some/ca/file',
        host: 'locahost',
        port: 8883,
        rejectUnauthorised: true
      }
    }
    await expect(forkChildProcess(args, './src/notify.js'))
      .rejects
      .toHaveProperty(
        'message',
        'Arguments are missing the object detection property'
      )
  })

  test('Error is raised for missing objectDetection.timeoutPeriod property', async () => {
    const args = {
      mqtt: {
        cafile: '/path/to/some/ca/file',
        host: 'locahost',
        port: 8883,
        rejectUnauthorised: true
      },
      objectDetection: {
      }
    }
    await expect(forkChildProcess(args, './src/notify.js'))
      .rejects
      .toHaveProperty(
        'message',
        'Missing objectDetection.timeoutPeriod argument'
      )
  })
})

describe('Process class', () => {
  jest.useFakeTimers()

  afterEach(() => {
    jest.clearAllTimers()
  })

  test('Process constructor initialises timer, ProcessArgs and mqttClient', () => {
    const expectedCaFile = '/usr/local/share/ca-certificates/CA.crt'
    const expectedMqttsUrl = 'mqtts://user:pass@localhost:8883'
    const expectedRejectUnauthorised = 'true'
    const expectedTimeoutPeriod = 5000

    const testProcess = new Process(['node', 'notify.js', expectedMqttsUrl, expectedCaFile, expectedRejectUnauthorised, expectedTimeoutPeriod])

    expect(testProcess).toBeDefined()
    expect(testProcess.constructor.name).toEqual('Process')
    expect(testProcess.args).toBeDefined()
    expect(testProcess.args.constructor.name).toEqual('ProcessArgs')
    expect(testProcess.args.args).toEqual(['node', 'notify.js', expectedMqttsUrl, expectedCaFile, expectedRejectUnauthorised, expectedTimeoutPeriod])
    expect(testProcess.args.mqttsUrl).toEqual('')
    expect(testProcess.args.cafile).toEqual('')
    expect(testProcess.args.timeoutPeriod).toEqual(0)
    expect(testProcess.args.rejectUnauthorised).toEqual(true)
    expect(testProcess.timers).toEqual({})
    expect(testProcess.mqttClient).toBeUndefined()
  })

  test('Process init method initialises arguments and mqttClient connection', async () => {
    const expectedCaFile = `${__dirname}/docker/certs/localCA.crt`
    const expectedMqttsUrl = 'mqtts://user:pass@localhost:8883'
    const expectedRejectUnauthorised = 'false'
    const expectedTimeoutPeriod = 5000

    const expectedArgs = [
      'node',
      'notify.js',
      expectedMqttsUrl,
      expectedCaFile,
      expectedRejectUnauthorised,
      expectedTimeoutPeriod
    ]

    const testProcess = new Process(expectedArgs)

    const argsMock = jest.spyOn(testProcess.args, 'init')

    try {
      await testProcess.init()

      expect(argsMock).toHaveBeenCalled()
      expect(testProcess.args.args).toEqual(expectedArgs)
      expect(testProcess.args.mqttsUrl).toEqual(expectedMqttsUrl)
      expect(testProcess.args.cafile).toEqual(expectedCaFile)
      expect(testProcess.args.timeoutPeriod).toEqual(expectedTimeoutPeriod)
      expect(testProcess.args.rejectUnauthorised).toEqual(false)
      expect(testProcess.timers).toEqual({})
      expect(testProcess.mqttClient).toBeDefined()
    } finally {
      argsMock.mockRestore()
    }
  })

  test('Process init method throws an error when arguments are invalid', async () => {
    const expectedArgs = ['arg1']
    const expectedErrorPayload = { type: 'INITIALISATION', success: false }

    const testProcess = new Process(expectedArgs)

    const argsMock = jest.spyOn(testProcess.args, 'init')
    const processSendMock = jest.spyOn(process, 'send').mockImplementation((obj) => { return obj })

    await testProcess.init()

    try {
      expect(argsMock).toHaveBeenCalled()
      expect(processSendMock).toHaveBeenCalled()
      expect(processSendMock.mock.results).toHaveLength(1)
      expect(processSendMock.mock.results[0].value).toHaveProperty('type', expectedErrorPayload.type)
      expect(processSendMock.mock.results[0].value).toHaveProperty('success', expectedErrorPayload.success)
      expect(processSendMock.mock.results[0].value).toHaveProperty('error')
    } finally {
      argsMock.mockRestore()
      processSendMock.mockRestore()
    }
  })

  test('Process error handler outputs error to error stream', () => {
    const testProcess = new Process([])

    const err = new Error('error')
    const consoleErrMock = jest.spyOn(console, 'error')

    try {
      testProcess.errorHandler(err)
      expect(consoleErrMock).toHaveBeenCalledTimes(1)
      expect(consoleErrMock).toHaveBeenCalledWith(err)
    } finally {
      consoleErrMock.mockRestore()
    }
  })

  test('Process error handler does not output undefined error object to error stream', () => {
    const testProcess = new Process([])

    const consoleErrMock = jest.spyOn(console, 'error')

    try {
      testProcess.errorHandler(undefined)
      expect(consoleErrMock).toHaveBeenCalledTimes(0)
    } finally {
      consoleErrMock.mockRestore()
    }
  })

  test('Process getMqttsUrl returns mqttsUrl process argument', async () => {
    const expectedCaFile = `${__dirname}/docker/certs/localCA.crt`
    const expectedMqttsUrl = 'mqtts://user:pass@localhost:8883'
    const expectedRejectUnauthorised = 'false'
    const expectedTimeoutPeriod = 5000

    const expectedArgs = [
      'node',
      'notify.js',
      expectedMqttsUrl,
      expectedCaFile,
      expectedRejectUnauthorised,
      expectedTimeoutPeriod
    ]

    const testProcess = new Process(expectedArgs)

    const argsMock = jest.spyOn(testProcess.args, 'init')

    try {
      await testProcess.init()

      const url = testProcess.getMqttsUrl()
      expect(url).toEqual(expectedMqttsUrl)
    } finally {
      argsMock.mockRestore()
    }
  })

  test('Process message handler sends a message notification for a camera monitor not yet logged with a timer', async () => {
    const args = [
      'node',
      'notify.js',
      'mqtts://user:pass@localhost:8883',
      `${__dirname}/docker/certs/localCA.crt`,
      'false',
      5000
    ]

    const data = { group: 'group', monitorId: 'monitorId' }
    const key = data.group + data.monitorId

    const expectedTopic = `shinobi/${data.group}/${data.monitorId}/trigger`

    const testProcess = new Process(args)
    await testProcess.init()

    const mqttPublishMock = jest.spyOn(testProcess.mqttClient, 'publish')

    try {
      await testProcess.messageHandler(data)

      expect(testProcess.timers[key]).toBeDefined()
      expect(testProcess.timers[key].running).toBeTruthy()

      expect(mqttPublishMock).toHaveBeenCalledWith(expectedTopic, JSON.stringify(data))
    } finally {
      mqttPublishMock.mockRestore()
    }
  })

  test('Process message handler sends a message notification for a camera monitor that has a timer but it is not running', async () => {
    const args = [
      'node',
      'notify.js',
      'mqtts://user:pass@localhost:8883',
      `${__dirname}/docker/certs/localCA.crt`,
      'false',
      5000
    ]

    const data = { group: 'notRunningGroup', monitorId: 'notRunningMonitorId' }
    const key = data.group + data.monitorId

    const expectedTopic = `shinobi/${data.group}/${data.monitorId}/trigger`

    const testProcess = new Process(args)
    testProcess.timers[key] = new Timer(data.monitorId, data.group, 5000)

    await testProcess.init()

    const mqttPublishMock = jest.spyOn(testProcess.mqttClient, 'publish')

    try {
      await testProcess.messageHandler(data)

      expect(testProcess.timers[key].running).toBeTruthy()
      expect(mqttPublishMock).toHaveBeenCalledWith(expectedTopic, JSON.stringify(data))
    } finally {
      mqttPublishMock.mockRestore()
    }
  })

  test('Process message handler discards message notification for a camera monitor that has a timer running', async () => {
    const args = [
      'node',
      'notify.js',
      'mqtts://user:pass@localhost:8883',
      `${__dirname}/docker/certs/localCA.crt`,
      'false',
      5000
    ]

    const data = { group: 'runningGroup', monitorId: 'runningMonitorId' }
    const key = data.group + data.monitorId

    const testProcess = new Process(args)
    testProcess.timers[key] = new Timer(data.monitorId, data.group, 5000)

    await testProcess.init()

    testProcess.timers[key].run()

    const mqttPublishMock = jest.spyOn(testProcess.mqttClient, 'publish')
    await testProcess.messageHandler(data)

    expect(mqttPublishMock).not.toHaveBeenCalled()
    expect(testProcess.timers[key].running).toBeTruthy()
  })

  test('Process message handler throws error when mqtt client fails to send detection when a timer is not running', async () => {
    const args = [
      'node',
      'notify.js',
      'mqtts://user:pass@localhost:8883',
      `${__dirname}/docker/certs/localCA.crt`,
      'false',
      5000
    ]

    const data = { group: 'notRunningGroup', monitorId: 'notRunningMonitorId' }
    const key = data.group + data.monitorId

    const expectedTopic = `shinobi/${data.group}/${data.monitorId}/trigger`

    const testProcess = new Process(args)
    testProcess.timers[key] = new Timer(data.monitorId, data.group, 1000)
    testProcess.timers[key].cancel()

    await testProcess.init()

    const mqttPublishMock = jest.spyOn(testProcess.mqttClient, 'publish').mockImplementation(
      (topic, message) => {
        throw new Error('Publish Error')
      }
    )

    try {
      await expect(testProcess.messageHandler(data))
        .rejects
        .toHaveProperty(
          'message',
          'Publish Error'
        )

      expect(testProcess.timers[key].running).toBeFalsy()
      expect(mqttPublishMock).toHaveBeenCalledWith(expectedTopic, JSON.stringify(data))
    } finally {
      mqttPublishMock.mockRestore()
    }
  })

  test('Process message handler throws an error when fails to send a detection for the first time for camera', async () => {
    const args = [
      'node',
      'notify.js',
      'mqtts://user:pass@localhost:8883',
      `${__dirname}/docker/certs/localCA.crt`,
      'false',
      5000
    ]

    const data = { group: 'notRunningGroup', monitorId: 'notRunningMonitorId' }
    const key = data.group + data.monitorId

    const expectedTopic = `shinobi/${data.group}/${data.monitorId}/trigger`

    const testProcess = new Process(args)

    await testProcess.init()

    const mqttPublishMock = jest.spyOn(testProcess.mqttClient, 'publish').mockImplementation(
      (topic, message) => {
        throw new Error('Publish Error')
      }
    )

    try {
      await expect(testProcess.messageHandler(data))
        .rejects
        .toHaveProperty(
          'message',
          'Publish Error'
        )

      expect(testProcess.timers).not.toHaveProperty(key)
      expect(mqttPublishMock).toHaveBeenCalledWith(expectedTopic, JSON.stringify(data))
    } finally {
      mqttPublishMock.mockRestore()
    }
  })

  test('Process message handler throws an error when mqttClient property is not defined', async () => {
    const data = { group: 'notRunningGroup', monitorId: 'notRunningMonitorId' }
    const key = data.group + data.monitorId

    const testProcess = new Process([])

    await expect(testProcess.messageHandler(data))
      .rejects
      .toHaveProperty(
        'message',
        'MQTT client not initialised'
      )

    expect(testProcess.timers).not.toHaveProperty(key)
  })

  test('Process dispose method cancels running timer instances', async () => {
    const args = [
      'node',
      'notify.js',
      'mqtts://user:pass@localhost:8883',
      `${__dirname}/docker/certs/localCA.crt`,
      'false',
      5000
    ]

    const data = { group: 'runningGroup', monitorId: 'runningMonitorId' }
    const key = data.group + data.monitorId

    const testProcess = new Process(args)
    testProcess.timers[key] = new Timer(data.monitorId, data.group, 5000)

    await testProcess.init()

    testProcess.timers[key].run()
    testProcess.dispose()

    expect(testProcess.timers[key].running).toEqual(false)
  })
})

describe('Process helper functions', () => {
  test('isDebugging returns true when --inspect-brk present on execArgv', () => {
    const existingExecArgs = [...process.execArgv]

    if (!isDebugging()) {
      process.execArgv.push('--inspect-brk')
    }

    try {
      const result = _testable.isDebugging()
      expect(result).toBeTruthy()
    } finally {
      process.execArgv = [...existingExecArgs]
    }
  })

  test('isDebugging returns false when --inspect-brk or --inspect is not present on execArgv', () => {
    const existingExecArgs = [...process.execArgv]

    if (isDebugging()) {
      const indx = process.execArgv.indexOf('--inspect-brk=9229')
      process.execArgv.splice(indx, 1)
    }

    try {
      const result = _testable.isDebugging()
      expect(result).toBeFalsy()
    } finally {
      process.execArgv = existingExecArgs
    }
  })

  test('getMqttUrl returns url that contains values of environment variables', () => {
    process.env.MQTT_USER = 'user'
    process.env.MQTT_PASSWORD = 'pass'
    process.env.MQTT_HOST = 'localhost'

    const expectedUrl = `mqtts://${process.env.MQTT_USER}:${process.env.MQTT_PASSWORD}@${process.env.MQTT_HOST}:8883`

    let url = ''
    try {
      url = _testable.getMqttUrl({})
    } finally {
      expect(url).toEqual(expectedUrl)
      delete process.env.MQTT_HOST
      delete process.env.MQTT_PASSWORD
      delete process.env.MQTT_USER
    }
  })

  test('getMqttUrl returns undefined url when env not defined', () => {
    expect(() => _testable.getMqttUrl({})).toThrow()
  })

  test('getMqttUrl returns url containing config values with user and password', () => {
    const config = {
      mqtt: {
        host: 'localhost',
        password: 'user',
        user: 'pass'
      }
    }
    const expectedUrl = `mqtts://${config.mqtt.user}:${config.mqtt.password}@${config.mqtt.host}:8883`

    const url = _testable.getMqttUrl(config)
    expect(url).toEqual(expectedUrl)
  })

  test('getMqttUrl throws error when config missing user and password and MQTT_USER and MQTT_PASSWORD environment vars set', () => {
    const config = {
      mqtt: {
        host: 'localhost'
      }
    }

    expect(() => { _testable.getMqttUrl(config) }).toThrow()
  })

  test('validateEnvironment returns true when all expected env vars present', () => {
    process.env.MQTT_USER = 'user'
    process.env.MQTT_PASSWORD = 'pass'
    process.env.MQTT_HOST = 'localhost'

    const expectedResult = true

    try {
      const result = _testable.validateEnvironment()
      expect(result).toEqual(expectedResult)
    } finally {
      delete process.env.MQTT_HOST
      delete process.env.MQTT_PASSWORD
      delete process.env.MQTT_USER
    }
  })

  test('validateEnvironment returns false when at least one environment var required is missing', () => {
    process.env.MQTT_USER = 'user'
    process.env.MQTT_HOST = 'localhost'

    const expectedResult = false

    try {
      const result = _testable.validateEnvironment()
      expect(result).toEqual(expectedResult)
    } finally {
      delete process.env.MQTT_HOST
      delete process.env.MQTT_USER
    }
  })
})

describe('Process module tests', () => {
  beforeEach(() => {
    jest.resetModules()
  })

  test('_testable defined when NODE_ENV = test', () => {
    const processModule = require('../src/libs/utils/src/lib/process')
    expect(processModule._testable).toBeDefined()
  })

  test('_testable defined when NODE_ENV != test', () => {
    const curNodeEnv = process.env.NODE_ENV

    process.env.NODE_ENV = 'dev'

    try {
      const processModule = require('../src/libs/utils/src/lib/process')

      expect(processModule._testable).toBeUndefined()
    } finally {
      process.env.NODE_ENV = curNodeEnv
    }
  })
})
