'use strict'

const notify = require('../src/notify')
const { Process } = require('../src/libs/utils/src/lib/process')

const mockProcess = {
  dispose: jest.fn(() => {
  }),
  errorHandler: jest.fn(() => {

  }),
  init: jest.fn(async () => {

  }),
  messageHandler: jest.fn((data) => {

  }),
  signalExit: jest.fn((exitCode, child) => {
  })
}

jest.mock('../src/libs/utils/src/lib/process', () => {
  return {
    Process: jest.fn((args) => { return mockProcess }),
    forkChildProcess: jest.fn()
  }
})

describe('notify tests', () => {
  beforeEach(() => {
    mockProcess.dispose.mockRestore()
    mockProcess.errorHandler.mockRestore()
    mockProcess.init.mockRestore()
    mockProcess.messageHandler.mockRestore()
    mockProcess.signalExit.mockRestore()
  })

  test('dispose calls dispose of child argument', () => {
    const p = new Process(['arg1'])
    notify._testable.dispose(p)
    expect(p.dispose).toHaveBeenCalledTimes(1)
    expect(p.dispose).toHaveReturnedWith(undefined)
  })

  test('dispose does not throw an error when pass undefined', () => {
    expect(() => { notify._testable.dispose(undefined) }).not.toThrow()
  })

  test('main function triggers process initilisation', () => {
    notify._testable.main()
    expect(mockProcess.init).toHaveBeenCalledTimes(1)
  })

  test('main function subscribes to message handler', () => {
    const events = {}

    const mockSystemProcess = jest.spyOn(process, 'on').mockImplementation((event, listener) => {
      events[event] = listener
      return process
    })

    try {
      notify._testable.main()
      expect(mockProcess.init).toHaveBeenCalledTimes(1)

      events.message('data')
      expect(mockProcess.messageHandler).toHaveBeenCalledTimes(1)
      expect(mockProcess.messageHandler).toHaveBeenCalledWith('data')
    } finally {
      mockSystemProcess.mockRestore()
    }
  })

  test('main function subscribes to error handler', () => {
    const mockError = new Error('error')
    const events = {}

    const mockSystemProcess = jest.spyOn(process, 'on').mockImplementation((event, listener) => {
      events[event] = listener
      return process
    })

    try {
      notify._testable.main()
      expect(mockProcess.init).toHaveBeenCalledTimes(1)

      events.error(mockError)
      expect(mockProcess.errorHandler).toHaveBeenCalledTimes(1)
      expect(mockProcess.errorHandler).toHaveBeenCalledWith(mockError)
    } finally {
      mockSystemProcess.mockRestore()
    }
  })

  test('main function subscribes to SIGTERM handler', () => {
    const events = {}
    const exitCode = 15
    const signal = 'SIGTERM'

    const mockExit = jest.spyOn(process, 'exit').mockImplementation(() => { })
    const mockSystemProcess = jest.spyOn(process, 'on').mockImplementation((event, listener) => {
      events[event] = listener
      return process
    })

    try {
      notify._testable.main()
      expect(mockProcess.init).toHaveBeenCalledTimes(1)

      events.SIGTERM(signal, exitCode)
      expect(mockExit).toHaveBeenCalledTimes(1)
      expect(mockExit).toHaveBeenCalledWith(exitCode + 128)
    } finally {
      mockExit.mockRestore()
      mockSystemProcess.mockRestore()
    }
  })

  test('main function subscribes to SIGINT handler', () => {
    const events = {}
    const exitCode = 2
    const signal = 'SIGINT'

    const mockExit = jest.spyOn(process, 'exit').mockImplementation(() => { })
    const mockSystemProcess = jest.spyOn(process, 'on').mockImplementation((event, listener) => {
      events[event] = listener
      return process
    })

    try {
      notify._testable.main()
      expect(mockProcess.init).toHaveBeenCalledTimes(1)

      events.SIGINT(signal, exitCode)
      expect(mockExit).toHaveBeenCalledTimes(1)
      expect(mockExit).toHaveBeenCalledWith(exitCode + 128)
    } finally {
      mockExit.mockRestore()
      mockSystemProcess.mockRestore()
    }
  })

  test('signalExit function disposes child and returns 128 + exit code', () => {
    const exitCode = 0
    const mockExit = jest.spyOn(process, 'exit').mockImplementation(() => { })
    const p = new Process(['arg1'])
    try {
      notify._testable.signalExit(exitCode, p)
      expect(mockExit).toHaveBeenCalledTimes(1)
      expect(mockExit).toHaveBeenCalledWith(exitCode + 128)
      expect(p.dispose).toHaveBeenCalledTimes(1)
      expect(p.dispose).toHaveReturnedWith(undefined)
    } finally {
      mockExit.mockRestore()
    }
    expect(p.dispose).toHaveBeenCalledTimes(1)
    expect(p.dispose).toHaveReturnedWith(undefined)
  })
})

describe('notify module tests', () => {
  beforeEach(() => {
    jest.resetModules()
  })

  test('testable functions exported when NODE_ENV is test', () => {
    const oldNodeEnv = process.env.NODE_ENV

    try {
      const notify = require('../src/notify')
      expect(notify._testable).toBeDefined()
    } finally {
      process.env.NODE_ENV = oldNodeEnv
    }
  })

  test('testable functions exported when NODE_ENV is not test', () => {
    const oldNodeEnv = process.env.NODE_ENV
    process.env.NODE_ENV = 'dev'

    try {
      const notify = require('../src/notify')
      expect(notify._testable).toBeUndefined()
    } finally {
      process.env.NODE_ENV = oldNodeEnv
    }
  })
})
