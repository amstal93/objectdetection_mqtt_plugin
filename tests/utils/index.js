'use strict'

/**
 * Helper utilities for testing
 */

const fs = require('fs').promises

/**
 * Promise that resolves when a message has been received a specific number
 * times.
 * @param {*} mqttClient - MQTT.js client
 * @param {*} expectedMessage - Message to count occurrences for
 * @param {string} expectedTopic - The topic message has been published for
 * @param {number} maxCount - Max count value before promise resolves
 */
async function countMessageReceivedAsync (mqttClient, expectedMessage, expectedTopic, maxCount) {
  return new Promise((resolve, reject) => {
    let count = 1
    mqttClient.on('message', function (topic, message) {
      try {
        const receivedMessage = JSON.parse(message.toString())

        expect(topic).toEqual(expectedTopic)
        expect(receivedMessage).toEqual(expectedMessage)

        count = count + 1
        if (maxCount === 1 || count === maxCount) {
          resolve()
        }
      } catch (err) {
        reject(err)
      }
    })
  })
}

/**
 * Dipose resources, disconnecting mqttClient and killing child process with SIGTERM
 * @param {*} mqttClient - mqttClient connection handle
 * @param {*} child - forked notify.js process
 */
async function dispose (mqttClient, child) {
  if (mqttClient) {
    await mqttClient.end()
  }
  if (child) {
    child.kill('SIGTERM')
  }
}

/**
 * Load object from JSON file asynchronously
 * @returns {Object} - Message
 */
async function loadJSON (filePath) {
  return JSON.parse(await fs.readFile(filePath))
}

/**
 * Promise that resolves when total number of seconds has elapsed
 * @param {number} seconds - Number of seconds to wait
 */
async function wait (seconds) {
  return new Promise((resolve) => {
    setTimeout(resolve, seconds * 1000)
  })
}

/**
 * Construct object from config file or environment variables.
 * The object contains the following properties:
 * - host
 * - user
 * - password
 * - mqttsUrl: uses properties host, user and password with port 8883
 * - mqttUrl: uses properties host, user and password with port 1883
 *
 * The host, user and password properties from
 * MQTT_HOST, MQTT_USER and MQTT_PASSWORD take precedence over the
 * config.host, config.user and config.password properties from the config
 * file
 * @param {Object} config - Object representing process configuration
 * @param {Object} config.mqtt - Object representing args for the MQTT broker
 * @param {string} config.mqtt.cafile - Absolute path to CA file
 * @param {string} config.mqtt.host - MQTT host
 * @param {string} config.mqtt.user - MQTT user
 * @param {string} config.mqtt.password - MQTT password
 * @returns {Object} Object containing host, password, user, mqttUrl and mqttsUrl
 * @throws {Error} When fails to locate credentials from config file or
 * environment variables
 */
function getMqttConfig (config) {
  const env = validateEnvironment()

  let mqttConfig = {}

  if (!env && config && config.mqtt.host && config.mqtt.password && config.mqtt.user) {
    mqttConfig = {
      host: config.mqtt.host,
      mqttUrl: `mqtt://${config.mqtt.user}:${config.mqtt.password}@${config.mqtt.host}:1883`,
      mqttsUrl: `mqtts://${config.mqtt.user}:${config.mqtt.password}@${config.mqtt.host}:8883`,
      password: config.mqtt.password,
      user: config.mqtt.user
    }
  } else if (env) {
    mqttConfig = {
      host: process.env.MQTT_HOST,
      mqttUrl: `mqtt://${process.env.MQTT_USER}:${process.env.MQTT_PASSWORD}@${process.env.MQTT_HOST}:1883`,
      mqttsUrl: `mqtts://${process.env.MQTT_USER}:${process.env.MQTT_PASSWORD}@${process.env.MQTT_HOST}:8883`,
      password: process.env.MQTT_PASSWORD,
      user: process.env.MQTT_USER
    }
  } else {
    throw new Error('Failed to construct mqtt url, include user and password properties in section of config file or set the MQTT_USER and MQTT_PASSWORD environment variables')
  }

  return mqttConfig
}

/**
 * Validate expected environment variables are set:
 * MQTT_PASSWORD MQTT_URL MQTT_USER
 * @returns True if required environment variables set, otherwise false
 */
function validateEnvironment () {
  return (process.env.MQTT_HOST && process.env.MQTT_PASSWORD && process.env.MQTT_USER)
}

module.exports = {
  countMessageReceivedAsync: countMessageReceivedAsync,
  dispose: dispose,
  getMqttConfig: getMqttConfig,
  loadJSON: loadJSON,
  wait: wait,
  validateEnvironment: validateEnvironment
}
