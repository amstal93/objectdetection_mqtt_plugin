'use strict'

const { Timer } = require('../src/libs/utils/src/lib/timer.js')

jest.useFakeTimers()

describe('Timer tests', () => {
  afterEach(() => {
    jest.clearAllTimers()
  })

  test('Timer constructed with monitorId, group and timeout arguments', () => {
    const expectedMonitorId = 'monitorId'
    const expectedGroupId = 'groupId'
    const expectedTimeout = 1000

    const timer = new Timer(expectedMonitorId, expectedGroupId, expectedTimeout)
    expect(timer).toBeDefined()
    expect(timer.constructor.name).toEqual('Timer')
    expect(timer.monitorId).toEqual(expectedMonitorId)
    expect(timer.groupId).toEqual(expectedGroupId)
    expect(timer.timeoutPeriod).toEqual(expectedTimeout)
    expect(timer.running).toBeFalsy()
    expect(timer.timeoutObj).toBeFalsy()
  })

  test('Timer running status is false after timer completes', async () => {
    const timer = new Timer('monitorId', 'groupId', 1000)
    timer.run()

    jest.runOnlyPendingTimers()

    return expect(timer.running).toBeFalsy()
  })

  test('Timer running status is true while timer is running', async () => {
    const timer = new Timer('monitorId', 'groupId', 3000)
    timer.run()

    return expect(timer.running).toBeTruthy()
  })

  test('Timer running status is true when timer is run a second time and the time period has not elapsed', async () => {
    const timer = new Timer('monitorId', 'groupId', 1000)
    timer.run()

    jest.advanceTimersByTime(500)

    timer.run()

    return expect(timer.running).toBeTruthy()
  })

  test('Timer cancels ', async () => {
    const timer = new Timer('monitorId', 'groupId', 5000)
    timer.run()

    jest.advanceTimersByTime(1000)
    timer.cancel()

    return expect(timer.running).toBeFalsy()
  })

  test('Timer can access running status of timer while it is running', async () => {
    const timer = new Timer('monitorId', 'groupId', 1000)
    timer.run()
    expect(timer.isRunning()).toBeTruthy()
  })

  test('Timer can access running status of timer after it has completed', async () => {
    const timer = new Timer('monitorId', 'groupId', 1000)
    timer.run()

    jest.runOnlyPendingTimers()

    expect(timer.isRunning()).toBeFalsy()
  })
})
