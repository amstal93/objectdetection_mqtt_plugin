'use strict'

const fork = require('child_process').fork

const { parentLogger } = require('./logger')
const mqttsConnect = require('./mqttClient.js')
const { Timer } = require('./timer.js')
const { ValidateProcess } = require('./validation.js')

const log = parentLogger.child({ module: 'process' })

/** @class Class to manage process arguments */
class ProcessArgs {
  /**
   * @constructor
   * Constructor that initilises process arguments as follows:
   * [ mqttsUrl = '', cafile = '', timeoutPeriod = 0, rejectUnauthorised = true ]
   * @param {Array<string>} - process.argv arguments
   */
  constructor (args) {
    /**
     * {string[]} Process arguments
     */
    this.args = args

    this.mqttsUrl = ''
    this.cafile = ''
    this.timeoutPeriod = 0
    this.rejectUnauthorised = true
  }

  /**
   * Initialise process arguments for mqttsUrl, cafile, timeoutPeriod and rejectUnauthorised
   * Process arguments are initialised from process.argv
   */
  async init () {
    let validatedArgs
    try {
      const validation = new ValidateProcess(this.args)
      validatedArgs = await validation.initArgs()
      this.mqttsUrl = validatedArgs.mqttsUrl
      this.cafile = validatedArgs.cafile
      this.timeoutPeriod = validatedArgs.timeoutPeriod
      this.rejectUnauthorised = validatedArgs.rejectUnauthorised

      log.debug(`Accepted arguments => url := ${this.mqttsUrl} :: caFile := ${this.cafile} :: rejectUnauthorised := ${this.rejectUnauthorised} :: timeoutPeriod := ${this.timeoutPeriod}`)
    } catch (err) {
      log.error(`${err}`)
      throw new Error(err)
    }

    return validatedArgs
  }
}

/** @class Class to manage this child process for sending messages to MQTT */
class Process {
  /**
   * Constructor that initialises process arguments.
   * Initialses an empty dictionary of timers to track messages dispatched for each camera.
   * @constructor
   * @param {Array.<string>} args - Process arguments include mqttsUrl, cafile, timeoutPeriod, rejectUnauthorised
   */
  constructor (argsv) {
    /**
     * @typedef ProcessArgs
     * @type {object}
     * @property {string} mqttsUrl - Url for mqtt broker that uses protocol mqtts.
     * @property {string} cafile - Path to certificate authority file for connecting to mqtt broker.
     * @property {number} timeoutPeriod - The timeout period in milliseconds.
     * @property {boolean} rejectUnauthorised - True if unauthorised ceritifcate authorities should be reject when using mqtts.
     */
    this.args = new ProcessArgs(argsv)

    /**
     * Dictionary of timers running, identified by groupId+monitorId
     * @type{{id: string, Timer}} Dictionary of timer instances identified by groupId+monitorId
     */
    this.timers = {}

    /**
     * Client for MQTT queue
     */
    this.mqttClient = undefined
  }

  /**
   * Dispose of process resources by cancelling the timers triggered per camera
   */
  dispose () {
    Object.keys(this.timers).forEach(key => {
      this.timers[key].cancel()
    })
    log.debug('Process dispose handler complete')
  }

  /**
   * Error handler
   * @param {Error} err - Error instance
   */
  errorHandler (err) {
    if (err) {
      console.error(err)
    }
  }

  /**
   * Initialise and validate the process arguments.
   * Despatches an 'INITIALISATION' message to the parent in completion
   * { type: 'INITIALISATION', error: undefined, success: false }
   * If an error is encountered while initialising despatches.....
   * { type: 'INITIALISATION', error: err, success: false }
   */
  async init () {
    try {
      await this.args.init()
      this.mqttClient = mqttsConnect(this.args.mqttsUrl, this.args.cafile, this.args.rejectUnauthorised)

      // send a message to parent as notification ready
      process.send({ type: 'INITIALISATION', error: undefined, success: true })
    } catch (err) {
      log.error(`${err}`)
      process.send({ type: 'INITIALISATION', error: err, success: false })
    }
  }

  getMqttsUrl () {
    return this.args.mqttsUrl
  }

  /**
   * Handle a message sent by parent process (shinobi-tensorflow.js)
   * @param {Message} data The message payload
   */
  async messageHandler (data) {
    const key = data.group + data.monitorId
    if (this.timers[key]) {
      log.debug(`Previously encountered a detection for camera ${data.group}\\${data.monitorId}`)
      if (!this.timers[key].isRunning()) {
        log.debug('Sending notification of detection via MQTT for this camera since timer is not currently running')

        try {
          await this._sendDetection(data)
          this.timers[key].run()
        } catch (error) {
          log.error(error)
          throw error
        }
      } else {
        log.debug('Skipping notification of detection for this camera, timer is currently running')
      }
    } else {
      log.debug('Sending notification of detection via MQTT for the first time for this camera')

      try {
        await this._sendDetection(data)
        const timer = new Timer(
          data.monitorId,
          data.group,
          this.args.timeoutPeriod
        )
        this.timers[key] = timer
        timer.run()
      } catch (error) {
        log.error(error)
        throw error
      }
    }
  }

  /**
   * Dispatch message to MQTT asynchronously
   * @param {Object} message - Message payload
   * @returns {Promise} Promise resolves when the publish operation
   * has been performed. The promise rejects when error encountered
   * during publishing or the mqtt client has not been initialised.
   */
  async _sendDetection (message) {
    if (this.mqttClient) {
      const topic = `shinobi/${message.group}/${message.monitorId}/trigger`

      this.mqttClient.publish(
        `${topic}`,
        JSON.stringify(message)
      )
    } else {
      throw new Error('MQTT client not initialised')
    }
  }
}

/** Port for inspect, each forked child increments this value */
let INSPECT_PORT = 5555

/**
 * Fork the child process and wait for it to initialise
 * Registers listeners for exit, error, message and uncaughtException events
 * @param {Object} config - Object representing process configuration
 * @param {Object} config.mqtt - Object representing args for the MQTT broker
 * @param {string} config.mqtt.cafile - Absolute path to CA file
 * @param {string} config.mqtt.host - MQTT host
 * @param {boolean} config.mqtt.rejectUnauthorised - Should untrusted CAs be
 * rejected. If this option is 'false' then could be subjected to
 * 'man in the middle' attacks
 * @param {string} config.mqtt.user - Username, optional. If omitted then
 * MQTT_USER environment variable should exist otherwise error thrown
 * @param {string} config.mqtt.password - Password, optional. If omitted then
 * MQTT_PASSWORD environment variable should exist otheriwse error thrown
 * @param {Object} config.objectDetection - Object detection configurarion args
 * @param {number} config.objectDetection.timeoutPeriod - Number of milliseconds
 * to wait between sending detection notifications on the queue
 * @param {string} scriptPath - Absolute path to script for child execution body
 * @returns {Promise} - Resolves with child process handle once a message has
 * been received from client for successful initialisation. If an
 * INITIALISATION message is received with an error property then the promise
 * is rejected
 */
async function forkChildProcess (config, scriptPath) {
  return new Promise((resolve, reject) => {
    try {
      const args = validateOptions(config)

      const forkOptions = { execArgv: [] }

      if (isDebugging()) {
        forkOptions.execArgv = [`--inspect-brk=${INSPECT_PORT++}`]
      }

      const child = fork(scriptPath, args, forkOptions)

      child.on('error', (err) => {
        reject(err)
      })

      child.on('exit', (code, signal) => {
        if (code) {
          if (code === 0) {
            resolve(code)
          } else {
            reject(new Error(`Child exiting with code ${code}`))
          }
        } else if (signal) {
          resolve(signal)
        }
      })

      child.on('uncaughtException', (err) => {
        resolve(err)
      })

      child.on('message', (childMessage) => {
        if (childMessage && !childMessage.type) {
          return
        }

        switch (childMessage.type) {
          case 'INITIALISATION': {
            if (childMessage.success) {
              resolve(child)
            } else {
              reject(childMessage.error)
            }
            break
          }
        }
      })
    } catch (error) {
      log.debug(`processChildProcess encountered an error => ${error}`)
      reject(error)
    }
  })
}

/**
 * Validate args object for inclusion of expected process configuration options
 * @param {Object} args - Object representing process configuration
 * @param {Object} args.mqtt - Object representing args for the MQTT broker
 * @param {string} args.mqtt.cafile - Absolute path to CA file
 * @param {string} args.mqtt.host - MQTT host
 * @param {number} args.mqtt.port - MQTT port
 * @param {string} args.mqtt.user - Username, if omitted then
 * MQTT_USER environment variable should exist otherwise error thrown
 * @param {string} args.mqtt.password - Password, if omitted then
 * MQTT_PASSWORD environment variable should exist otheriwse error thrown
 * @param {boolean} args.mqtt.rejectUnauthorised - Should untrusted CAs be
 * rejected. If this option is 'false' then could be subjected to
 * 'man in the middle' attacks
 * @param {Object} args.objectDetection - Object representing object detection args
 * @param {number} args.objectDetection.timeoutPeriod - Number of milliseconds
 * to wait between sending detection notifications on the queue
 * @returns {Array.<string>} - Process arguments arranged as an array of strings
 * according to [Process constructor]{@link Process#constructor}
 * @throws {Error} - When a required argument is missing
 * @see Process
 */
function validateOptions (args) {
  if (!args) {
    throw new Error('Arguments are empty')
  } else if (!args.mqtt) {
    throw new Error('Arguments are missing the mqtt property')
  } else if (!args.objectDetection) {
    throw new Error('Arguments are missing the object detection property')
  } else if (!args.mqtt.cafile) {
    throw new Error('Missing mqtt.cafile argument')
  } else if (!args.mqtt.host) {
    throw new Error('Missing mqtt.host argument')
  } else if (!args.mqtt.rejectUnauthorised) {
    throw new Error('Missing mqtt.rejectUnauthorised argument')
  } else if (!args.objectDetection.timeoutPeriod) {
    throw new Error('Missing objectDetection.timeoutPeriod argument')
  }

  return [
    getMqttUrl(args),
    args.mqtt.cafile,
    args.mqtt.rejectUnauthorised,
    args.objectDetection.timeoutPeriod
  ]
}

/**
 * Validate required environment variables are present
 * @returns - False if any of the following environment variables are missing
 * - MQTT_HOST
 * - MQTT_PASSWORD
 * - MQTT_USER
 * Otherwise returns true
 */
function validateEnvironment () {
  const envVars = ['MQTT_HOST', 'MQTT_PASSWORD', 'MQTT_USER']

  let valid = true

  let indx = 0
  while (valid && indx < envVars.length) {
    if (!process.env[envVars[indx]]) {
      valid = false
    }
    indx++
  }

  return valid
}

/**
 * Test if this process is being inspected by inspecting execArgv
 * @returns {boolean} True if this process is being inspected
 */
function isDebugging () {
  let count = 0
  let found = false

  const re = /^--(?:inspect-brk|inspect){1}(?:=\d+)?$/

  while (count < process.execArgv.length && !found) {
    if (re.test(process.execArgv[count])) {
      found = true
    }
    count++
  }

  return found
}

/**
 * Construct mqtt connection url from configuration object
 * The user and password properties from
 * MQTT_HOST, MQTT_USER and MQTT_PASSWORD take precedence over the
 * config.host, config.user and config.password properties
 * @param {Object} config - Object representing process configuration
 * @param {Object} config.mqtt - Object representing args for the MQTT broker
 * @param {string} config.mqtt.host - MQTT host
 * @param {string} config.mqtt.password - MQTT password
 * @param {string} config.mqtt.user - MQTT user
 * @returns {string} Mqtt url containing host, port and credentials
 * @throws {Error} When fails to locate credentials from config file or
 * environment variables
 */
function getMqttUrl (config) {
  const env = validateEnvironment()

  let url = ''

  if (!env && config && config.mqtt && (!config.mqtt.user || !config.mqtt.password)) {
    throw new Error('Failed to construct mqtt connection string: user and password properties not set in mqtt section of config file and MQTT_HOST, MQTT_USER and MQTT_PASSWORD environment variables are not set')
  } else if (!env && config && config.mqtt && config.mqtt.host && config.mqtt.user && config.mqtt.password) {
    url = `mqtts://${config.mqtt.user}:${config.mqtt.password}@${config.mqtt.host}:8883`
  } else if (env) {
    url = `mqtts://${process.env.MQTT_USER}:${process.env.MQTT_PASSWORD}@${process.env.MQTT_HOST}:8883`
  } else {
    throw new Error('Failed to configure mqtt connection string: No mqtt section in config file could be located and environment variables MQTT_HOST, MQTT_USER and MQTT_PASSWORD are not set')
  }

  return url
}

module.exports = {
  Process,
  forkChildProcess
}

if (process.env.NODE_ENV === 'test') {
  module.exports._testable = {
    isDebugging: isDebugging,
    getMqttUrl: getMqttUrl,
    validateEnvironment: validateEnvironment
  }
}
