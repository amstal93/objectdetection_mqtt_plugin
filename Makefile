#
# Docker Images:
#
#	livecam/plugin:								Intended for use on CI server. The plugin's package dependencies are
#												installed with source code and tests mounted as volumes.
#
#	livecam/debian:base:						Base image that contains curl, jq and bash:io library.
#	
#	livecam/mqtt_ping:							NodeJS http server that provides a endpoint at http://0.0.0.0:8080/ping
#												that tries to connect to an MQTT broker. Returns status 200 if 
#												connected, otherwise returns status 404. Provides an endpoint for the 
#												start-server-and-test utility in test:e2e script. This starts the 
#												docker-compose stack in tests/docker-compose.yml and will start the
#												e2e tests when the http://0.0.0.0:8080/ping endpoint returns status 200.
#
#	livecam/shinobi:							Image that contains an installation of Shinobi and the plugin.
#												Configuration files, streams and videos are mounted in 
#												docker/volumes/shinobi.
#
# Docker-Compose:
#
#	docker/docker-compose.yml:					Base file that contains the mqtt service.
#	docker/docker-compose.dev.yml:				Intended for use on local development machine. Offers full stack virtual 
#												environment: mqsl, nginx, shinobi in addition to the mqtt service included
#												within the base docker-compose file.
#	docker/docker-compose.ci.yml:				Intended for use on CI server. Offers plugin installed with source and
#												tests mounted as volumes. Provides the ability to build, test and perform
#												linting.
#	tests/docker/docker-compose.yml:			Intended for use when running tests locally. Provides an mqtt broker at
#												mqtts://<username>:<password>@localhost:8883. Also exposes ports
#												1883 and 9001 to host environment.
#
# Variables:
#
# 	BASE_DIR:									Static value of 'docker', i.e. the subfolder where compose files exist
#	BASE_IMAGE_DOCKER_FILE 						Static value of docker/Dockerfile.base.debian, Dockerfile for building 
#												image containing shinobi and the plugin
#	BASE_BUILD_ID								Static value of livecam/base, small base utility image
#	PING_BUILD_ID 								Static value of livecam/mqtt_ping, the ping image tag name
#	PING_DOCKER_FILE							Static value of tests/docker/ping/Dockerfile, the ping Dockerfile
#
# Make Rules:
#
#	all:										Build livecam/shinobi, livecam/base:debian, livecam/mqtt_ping and
#												livecam/plugin images.
#
#   build_dev:                                	Build image livecam/shinobi with source for plugin integrated and
#												configured. Intended for use on local development environment to
#												provide virtual environment, e.g. before committing changes into
#												source control.
#
#   build_base:									Build base helper images:
#												- docker/Dockerfile.base.debian
#												- tests/docker/ping/Dockerfile
#
#	clean:										Remove livecam/shinobi, livecam/base:debian, livecam/mqtt_ping and
#												livecam/plugin images. Prune all images and containers.
#
#   clean_build:                             	Prune docker images and docker containers.
#
#   down_ci:                                    Stop docker-compose containers for appropriate build environment.
#   down_dev:
#
#	up_dev:										Start mqtt, mysql, nginx and shinobi containers on local development machine. 
#												Intended for use as a virtual environment to test plugin is operational
#												before commiting to remote repository.
#

BASE_DIR := docker

BASE_IMAGE_DOCKER_FILE := docker/Dockerfile.base.debian
BASE_BUILD_ID := livecam/base
PING_BUILD_ID := livecam/mqtt_ping
PING_DOCKER_FILE := tests/docker/ping/Dockerfile 


.PHONY: build_base build_dev clean clean_build down_ci down_dev up_dev

all: build_base build_dev

build_base:
	docker build --pull \
		--build-arg BUILD_ID=${PING_BUILD_ID} \
		--tag livecam/mqtt_ping \
		--force-rm \
		-f ${PING_DOCKER_FILE} \
		tests/docker/ping
	
	docker image prune -f --filter label=stage=builder --filter label=build=${PING_BUILD_ID}
	docker build --pull \
		--build-arg BUILD_ID=${BASE_BUILD_ID} \
		--tag livecam/base:debian \
		--force-rm \
		-f ${BASE_IMAGE_DOCKER_FILE} \
		.

	docker image prune -f --filter label=build=${BASE_BUILD_ID}
	
build_dev:
	docker-compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml\
		--env-file=${CURDIR}/docker/.env\
		build

clean:
	docker rmi livecam/shinobi livecam/base:debian livecam/mqtt_ping
	${MAKE} clean_build

clean_build:
	docker image prune --force
	docker container prune --force

down_ci:
	docker-compose -f ${BASE_DIR}/docker-compose.yml -f ${BASE_DIR}/docker-compose.ci.yml down

down_dev:
	docker-compose -f ${BASE_DIR}/docker-compose.yml -f ${BASE_DIR}/docker-compose.dev.yml down

up_dev: build_dev
	docker-compose -f ${BASE_DIR}/docker-compose.yml -f ${BASE_DIR}/docker-compose.dev.yml \
		--env-file=${CURDIR}/docker/.env \
		up
