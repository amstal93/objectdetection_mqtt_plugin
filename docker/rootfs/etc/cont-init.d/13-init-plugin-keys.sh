#!/usr/bin/with-contenv bash
# ==============================================================================
# Link plugin config keys to main shinobi config
# ==============================================================================
# shellcheck disable=SC1091
source /usr/lib/bashio/bashio.sh

bashio::log.info "Initialising plugin keys..."

# synchronise plugin key in objectdetection plugin conf file and shinobi conf file
# as enforced by shinobi
SHINOBI_BASE=/opt/shinobi
node "${SHINOBI_BASE}/tools/modifyConfigurationForPlugin.js" objectdetection key=$(head -c 64 < /dev/urandom | sha256sum | awk '{print substr($1,1,60)}')
