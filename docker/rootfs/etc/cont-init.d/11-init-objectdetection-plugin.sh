#!/usr/bin/with-contenv bash
# ==============================================================================
# Creates tensorflow config file if it does not already exist
# ==============================================================================
# shellcheck disable=SC1091
source /usr/lib/bashio/bashio.sh

PLUGINS_BASE=/opt/shinobi/plugins

bashio::log.info 'Initialising objectdetection plugin...'

# create directory for objectdetection config
if ! bashio::fs.directory_exists '/data/plugins/objectdetection'; then
    bashio::log.info 'Create tensorflow directory structure...'
    mkdir -p '/data/plugins/objectdetection'
fi


# create default config file for objectdetection
if ! bashio::fs.file_exists '/data/plugins/objectdetection/conf.json'; then
    bashio::log.info 'Creating default tensorflow config file...'
    cp "${PLUGINS_BASE}/objectdetection/conf.sample.json" /data/plugins/objectdetection/conf.json
fi

# Synchronise config.json tfjsBuild key to reflect build
# if gpu tensorflow build then tfjsBuild => gpu, otherwise tfjsBuild => cpu
bashio::log.info 'Synchronising tfjsBuild with current build...'
BUILD_FILE="/opt/shinobi/plugins/objectdetection/build.json"
PLUGIN_CONF_FILE="/data/plugins/objectdetection/conf.json"

GPU=$(jq --raw-output '.gpu // empty' "${BUILD_FILE}")
if [ "${GPU}" -eq "1" ]; then
    contents="$(jq --raw-output '.tfjsBuild = "gpu"' "${PLUGIN_CONF_FILE}")"
else
    contents="$(jq --raw-output '.tfjsBuild = "cpu"' "${PLUGIN_CONF_FILE}")"
fi

echo "${contents}" > /data/plugins/objectdetection/conf.json

# Check that the MQTT CA file exists
config=$(</data/plugins/objectdetection/conf.json)
caFile=$(bashio::jq "${config}" ".mqtt.cafile")

bashio::log.info "Validating that MQTT broker CA file ${caFile} exists..."

if bashio::fs.file_exists ${caFile}; then
    bashio::log.info 'File is OK...'
else 
    bashio::log.fatal \
        'MQTT CA file does not exist...'
    bashio::exit.nok \
        "Please ensure it is created..."
fi

# Display plugin build configuration
BUILD="$(<${PLUGINS_BASE}/objectdetection/build.json)"
bashio::log.info "Plugin configured with build"
echo "${BUILD}"