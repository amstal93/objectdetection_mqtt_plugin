#!/usr/bin/with-contenv bash
# ==============================================================================
# Created a symlinks for shinobi plugins and pm2 configuration
# expects it to be.
# ==============================================================================
# shellcheck disable=SC1091
source /usr/lib/bashio/bashio.sh

bashio::log.info "Creating sym links to configuration files..."

ln -s /data/conf.json /opt/shinobi/conf.json
ln -s /data/super.json /opt/shinobi/super.json
ln -s /data/pm2Shinobi.yml /opt/shinobi/pm2Shinobi.yml

ln -s /data/plugins/objectdetection/conf.json /opt/shinobi/plugins/objectdetection/conf.json 

