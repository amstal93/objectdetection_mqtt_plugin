# ----------------------------------------------------------------------------
# Docker NodeJS Debian slim image providing the following packages:
# - ca-certificates curl jq
# 
# Downloads the following utilities:
# - S6 overlay for use in docker containers
# - Bashio bash functions from home-assistant
#
# Intended for use as a base image for NodeJS projects
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
# Image
# ----------------------------------------------------------------------------
ARG BUILD_VERSION=12-stretch-slim
FROM node:$BUILD_VERSION


# ----------------------------------------------------------------------------
# Enivonment
# ----------------------------------------------------------------------------
ENV \
    LANG="C.UTF-8" \
    S6_BEHAVIOUR_IF_STAGE2_FAILS=2 \
    S6_CMD_WAIT_FOR_SERVICES=1


# ----------------------------------------------------------------------------
# Shell
# ----------------------------------------------------------------------------
SHELL ["/bin/bash", "-o", "pipefail", "-c"]


# ----------------------------------------------------------------------------
# Build Arguments
# ----------------------------------------------------------------------------
ARG BASHIO_VERSION=0.9.0
ARG S6_OVERLAY_VERSION=1.22.1.0


# ----------------------------------------------------------------------------
# Base system
# ----------------------------------------------------------------------------
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    bzip2 ca-certificates curl jq \
    \
    && curl -L -s "https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-amd64.tar.gz" | tar zxvf - -C / \
    && mkdir -p /etc/fix-attrs.d \
    && mkdir -p /etc/services.d \
    \
    && mkdir -p /tmp/bashio \
    && curl -L -s "https://gitlab.com/cctv_web_cam/libs/bashio/-/archive/v0.9.0/bashio-v0.9.0.tar.bz2" | tar --strip-components=1 -jxvf - -C /tmp/bashio \
    && mv /tmp/bashio/lib /usr/lib/bashio \
    && ln -s /usr/lib/bashio/bashio /usr/bin/bashio \
    && rm -rf /tmp/bashio \
    \
    && apt-get remove -y bzip2 \
    && apt-get clean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*


# ----------------------------------------------------------------------------
# Metadata
# ----------------------------------------------------------------------------
ARG BUILD_ID
LABEL build=$BUILD_ID


# ----------------------------------------------------------------------------
# S6 overlay
# ----------------------------------------------------------------------------
ENTRYPOINT ["/init"]
