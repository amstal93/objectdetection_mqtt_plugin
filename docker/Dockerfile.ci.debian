# --------------------------------------------------------------------------
# This Dockerfile performs the following build actions:
#
# - Build package dependencies for this plugin
# - Declare volumes for src and tests
# When the container is started perform tests
# --------------------------------------------------------------------------


# --------------------------------------------------------------------------
# Build the dependencies for this plugin
# --------------------------------------------------------------------------
FROM livecam/base:debian as builder

ARG BUILD_GPU=0
ARG BUILD_ARCH='none'

WORKDIR /build/plugin

COPY .eslintrc.js /build/plugin
COPY package.json /build/plugin
COPY docker/build/buildPlugin.sh /build

RUN apt-get update \
    \
    && apt-get install -y --no-install-recommends \
    g++ git make python \
    \
    && chmod +x /build/*.sh \
    && echo "Building plugin with build arguments :: gpu=${BUILD_GPU}, arch=${BUILD_ARCH}" \
    && bash /build/buildPlugin.sh \
    \
    && rm -f /build/*.sh \
    && apt-get remove -y --purge g++ git make pkg-config python \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*


# --------------------------------------------------------------------------
# Volumes
# --------------------------------------------------------------------------
VOLUME [ "/build/plugin/tests" ]
VOLUME [ "/build/plugin/tests/coverage" ]
VOLUME [ "/build/plugin/src" ]
